import { Coord } from "./coord";

export class GameBoard {
  private board: (string | null)[];
  public readonly size: Coord;
  public readonly player1Id: string;
  public readonly player2Id: string;

  public constructor(player1Id: string, player2Id: string) {
    // default board
    const defaultBoard = []
      .concat([player1Id, null, null, player2Id])
      .concat([null, player1Id, player2Id, null])
      .concat([null, player2Id, player1Id, null])
      .concat([player2Id, null, null, player1Id]);
    const testBoard = []
      .concat([player1Id, null, player2Id, player2Id])
      .concat([player1Id, null, player2Id, null])
      .concat([player1Id, null, null, null])
      .concat([null, player1Id, null, player2Id]);
    this.board = testBoard;
    this.size = new Coord(4, 4);
    this.player1Id = player1Id;
    this.player2Id = player2Id;
  }
  public move(source: Coord, dest: Coord): void {
    const isP1 = this.get(source);
    if (isP1 === null)
      throw `Source ${source} points to an empty space`;
    this.set(source, null);
    this.set(dest, isP1);
  }
  public get(coord: Coord): boolean | null {
    const index = this.coordToBoardIndex(coord);
    if (!Number.isSafeInteger(index) || index < 0 || index >= this.board.length)
      throw `Index ${index} is out of bounds`;
    const id = this.board[index];
    const isP1 = id === null ? null : id === this.player1Id;
    return isP1;
  }
  public getCoords(id: string): Coord[] {
    const coords = this.board
      .map((pieceId, index) => {
        const isTarget = pieceId === id;
        return isTarget ? this.boardIndexToCoord(index) : null;
      })
      .filter((coord) => coord !== null);
    return coords;
  }
  private set(coord: Coord, isP1: boolean | null): void {
    const index = this.coordToBoardIndex(coord);
    if (!Number.isSafeInteger(index) || index < 0 || index >= this.board.length)
      throw `Index ${index} is out of bounds`;
    this.board[index] = isP1 === null ? null : isP1 ? this.player1Id : this.player2Id;
  }
  public coordToBoardIndex(coord: Coord): number {
    const { row, col } = coord;
    const index = row * this.size.col + col;
    return index;
  }
  private boardIndexToCoord(index: number): Coord {
    const col = index % this.size.col;
    const row = Math.floor(index / this.size.col);
    return new Coord(row, col);
  }
}
