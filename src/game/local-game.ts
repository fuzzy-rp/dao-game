import { Game, IGame, MoveResult } from './game';
import { ListenerManager } from '../utils/listeners';
import { showModal } from '../utils/modal';
import { Coord } from './coord';

const classNames = {
  selectedPiece: 'selected-piece',
  selectedPlayerText: 'selected-player-text',
  activePlayerPiece: 'active-player-piece',
  possibleMove: 'possible-move',
};

export class LocalGame extends Game {
  selectedPiece: Element | null = null;
  listenerManager: ListenerManager = new ListenerManager();

  // DOM references
  readonly dom = {
    defaultP1Piece: document.getElementById('default-p1-piece'),
    defaultP2Piece: document.getElementById('default-p2-piece'),
    resetButton: document.getElementById('reset-button') as HTMLInputElement,
    undoButton: document.getElementById('undo-button') as HTMLInputElement,
    cells: () => Array.from(document.querySelectorAll('td')),
    allPieces: () => Array.from(document.querySelectorAll('.piece')),
    p1TurnText: () => Array.from(document.querySelectorAll('.p1-turn-text'))[0],
    p2TurnText: () => Array.from(document.querySelectorAll('.p2-turn-text'))[0],
  };

  private getPiecesAndCoords(playerId: string) {
    const allCells = this.dom.cells();
    const playerPositions = this.getPlayerPositions(playerId);
    const piecesAndPositions = playerPositions.map((coord) => {
      const index = this.coordToBoardIndex(coord);
      const cell = allCells[index];
      const piece = cell.children[0];
      return { coord, piece };
    });
    return piecesAndPositions;
  }

  /**
   * Animates the motion of piece moving from source to dest
   * @param source Starting Coord
   * @param dest Ending Coord
   * @param playerId Player id
   */
  private async movePiece(source: Coord, dest: Coord) {
    const { dom } = this;
    const sourceIndex = this.coordToBoardIndex(source);
    const destIndex = this.coordToBoardIndex(dest);

    // find current piece
    const piece = dom.cells()[sourceIndex].children[0];
    // unselect piece
    piece.classList.remove(classNames.selectedPiece);

    // create clone for new position
    const destPiece = piece.cloneNode(true);
    // add to destination cell
    dom.cells()[destIndex].appendChild(destPiece);

    // start animations
    const animDurationMs = 500;
    const destElement = dom.cells()[destIndex];
    const destAnimation = destElement.animate([{ opacity: 0 }, { opacity: 1 }], { duration: animDurationMs, iterations: 1 });
    const sourceAnimation = piece.animate(
      [
        {
          opacity: 1,
          width: '45px',
          height: '45px',
        },
        {
          opacity: 0,
          width: '5px',
          height: '5px',
        },
      ],
      { duration: animDurationMs, iterations: 1 }
    );

    // await animations
    await Promise.all([sourceAnimation.finished, destAnimation.finished]);

    // remove old piece
    piece.parentNode.removeChild(piece);
  }

  public constructor(player1Id: string, player1Name: string, player2Id: string, player2Name: string) {
    super(player1Id, player1Name, player2Id, player2Name);
    this.init();
  }

  public override async makeMove(source: Coord, dest: Coord): Promise<MoveResult> {
    this.listenerManager.removeListeners();
    await this.movePiece(source, dest);
    const result = await super.makeMove(source, dest);
    this.handleMoveResult(result);
    return result;
  }

  public async handleMoveResult(result: MoveResult) {
    const { isTerminal, message } = result;
    if (isTerminal) {
      await showModal({ text: message });
      document.location.reload();
    } else {
      this.resetUi();
      this.setActiveUi();
    }
  }

  public resetUi() {
    const { dom, currentPlayerId, player1Id, canUndoMove, moveHistory, listenerManager, selectedPiece } = this;

    // reset text
    dom.p1TurnText().classList.remove(classNames.selectedPlayerText);
    dom.p2TurnText().classList.remove(classNames.selectedPlayerText);

    const isP1Turn = currentPlayerId === player1Id;
    if (isP1Turn) {
      dom.p1TurnText().classList.add(classNames.selectedPlayerText);
    } else {
      dom.p2TurnText().classList.add(classNames.selectedPlayerText);
    }

    if (dom.undoButton || dom.resetButton) {
      dom.undoButton.disabled = canUndoMove;
      if (moveHistory.length === 0) {
        dom.undoButton.disabled = true;
        dom.resetButton.disabled = true;
      } else {
        dom.undoButton.disabled = false;
        dom.resetButton.disabled = false;
      }
    }

    this.selectedPiece = null;

    // reset Cells
    dom.cells().forEach((cell) => cell.classList.remove(classNames.possibleMove));
    // reset pieces
    dom.allPieces().forEach((piece) => {
      piece.classList.remove(classNames.activePlayerPiece);
      piece.classList.remove(classNames.selectedPiece);
    });
    // remove all listeners
    listenerManager.removeListeners();
  }
  public setActiveUi() {
    const { dom, currentPlayerId, listenerManager } = this;

    const clearPossibleMovesBlink = () =>
      dom.cells().forEach((cell) => cell.classList.remove(classNames.possibleMove));
    const activePieceListener = (piece: Element, source: Coord) => {
      // clear current selection
      if (this.selectedPiece !== null) {
        this.selectedPiece.classList.remove(classNames.selectedPiece);
        clearPossibleMovesBlink();
        listenerManager.removeListeners();
        hookupActivePieces();
      }

      // set as selected
      this.selectedPiece = piece;
      piece.classList.add(classNames.selectedPiece);

      // hook up allowed moves for:
      // 1. blink
      // 2. complete the move
      const allowedMoves = this.getValidMoves(source);
      allowedMoves.forEach((dest: Coord) => {
        const cell = dom.cells()[this.coordToBoardIndex(dest)];
        cell.classList.add(classNames.possibleMove);
        listenerManager.addListener(cell, 'click', async () => {
          clearPossibleMovesBlink();
          await this.makeMove(source, dest);
        });
      });
    };

    const hookupActivePieces = () => {
      // mark active pieces
      const piecesAndPositions = this.getPiecesAndCoords(currentPlayerId);
      piecesAndPositions.forEach(({ coord: source, piece }) => {
        piece.classList.add(classNames.activePlayerPiece);
        listenerManager.addListener(piece, 'click', () => activePieceListener(piece, source));
      });
    }

    hookupActivePieces();
  }

  private init() {
    const { dom, player1Id, player1Name, player2Id, player2Name, size } = this;

    // create board and pieces
    let isWhite = true;
    const table = document.getElementById('table');
    for (let r = 0; r < size.row; r++) {
      const tr = document.createElement('tr');
      table.appendChild(tr);
      for (let c = 0; c < size.col; c++) {
        const td = document.createElement('td');
        tr.appendChild(td);
        td.classList.add('tile');
        td.classList.add(isWhite ? 'white' : 'black');
        isWhite = !isWhite;
      }
      isWhite = !isWhite;
    }
    const createPiece = (isP1Turn: boolean, pieceId: string, coord: Coord) => {
      const defaultPiece = isP1Turn ? dom.defaultP1Piece : dom.defaultP2Piece;
      const newPiece = defaultPiece.cloneNode(true) as HTMLParagraphElement;
      newPiece.id = pieceId;
      const index = this.coordToBoardIndex(coord);
      dom.cells()[index].appendChild(newPiece);
    };
    this.getPlayerPositions(player1Id).forEach((coord) => createPiece(true, player1Id, coord));
    this.getPlayerPositions(player2Id).forEach((coord) => createPiece(false, player2Id, coord));

    dom.p1TurnText().textContent = `${player1Name}`
    dom.p2TurnText().textContent = `${player2Name}`

    // hook up buttons
    let blockActions = false;
    if (dom.resetButton) {
      dom.resetButton.addEventListener('click', () => {
        if (blockActions) {
          return;
        }

        blockActions = true;
        if (confirm('Reset?')) {
          window.location.reload();
        }
        blockActions = false;
      });
    }

    if (dom.undoButton) {
      dom.undoButton.addEventListener('click', async () => {
        if (blockActions) {
          return;
        }

        blockActions = true;

        const move = this.undoMove();
        await this.movePiece(move.dest, move.source);
        this.resetUi();
        this.setActiveUi();

        blockActions = false;
      });
    }
  }
}

// start!
export const playGame = () => {
  const lg = new LocalGame('red-player', 'Red', 'blue-player', 'Blue');
  lg.resetUi();
  lg.setActiveUi();
};
