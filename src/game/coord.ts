export class Coord {
    public row: number;
    public col: number;

    constructor(row: number, col: number) {
        this.row = row;
        this.col = col;
    }

    public add(coord: Coord): Coord {
        return new Coord(this.row + coord.row, this.col + coord.col);
    }

    sub(coord: Coord): Coord {
        return new Coord(this.row - coord.row, this.col - coord.col);
    }

    /**
     * Finds if otherCoord is contained within a rectangle that sits between origin and this Coord
     * @param otherCoord
     */
    contains(otherCoord: Coord): boolean {
        const xRatio = otherCoord.col / this.col;
        const yRatio = otherCoord.row / this.row;
        const xyInsideSquare = xRatio >= 0 && xRatio < 1 && yRatio >= 0 && yRatio < 1;
        return xyInsideSquare;
    }

    equals(otherCoord: Coord): boolean {
        const areEqual = this.col === otherCoord.col && this.row === otherCoord.row;
        return areEqual;
    }

    // clock face directions
    static get allDirections(): Coord[] {
        const allDirections = [
            new Coord(1, 0), // up
            new Coord(1, 1), // up-right
            new Coord(0, 1), // right
            new Coord(-1, 1), // right-down
            new Coord(-1, 0), // down
            new Coord(-1, -1), // down-left
            new Coord(0, -1), // left
            new Coord(1, -1), // left-up
        ];
        return allDirections;
    }
}
