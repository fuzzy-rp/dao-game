import { GameBoard } from "./game-board";
import { Coord } from "./coord";

export interface Move {
  source: Coord;
  dest: Coord;
  playerId: string;
}

export interface MoveResult {
  isTerminal: boolean;
  message?: string;
}

export interface IGame {
  size: Coord;
  moveHistory: Move[];
  player1Id: string;
  player1Name: string;
  player2Id: string;
  player2Name: string;
  currentPlayerId: string;
  canUndoMove: boolean;
  getPlayerPositions: (playerId: string) => Coord[];
  coordToBoardIndex: (coord: Coord) => number;
  getValidMoves: (start: Coord) => Coord[];
  makeMove: (source: Coord, dest: Coord) => Promise<MoveResult>;
  undoMove: () => Move;
  getPiece(pos: Coord): string | undefined;
}

export class Game implements IGame {
  private isGameOver: boolean;
  private readonly gameBoard: GameBoard;
  private readonly _moveHistory: Move[];
  public readonly player1Id: string;
  public readonly player2Id: string;
  public readonly player1Name: string;
  public readonly player2Name: string;

  public constructor(player1Id: string, player1Name: string, player2Id: string, player2Name: string) {
    this.gameBoard = new GameBoard(player1Id, player2Id);
    this.player1Id = player1Id;
    this.player2Id = player2Id;
    this.player1Name = player1Name;
    this.player2Name = player2Name;
    this.isGameOver = false;
    this._moveHistory = [];
    this.currentPlayerId = player1Id;
  }
  getPiece(pos: Coord): string {
    const value = this.gameBoard.get(pos);
    if (value === null) return null;
    const b = value.valueOf();
    const id = b ? this.player1Id : this.player2Id;
    return id;
  }
  public currentPlayerId: string;
  public get size(): Coord {
    return this.gameBoard.size;
  }
  public get moveHistory(): Move[] {
    return this._moveHistory;
  }
  public getPlayerPositions(playerId: string): Coord[] {
    return this.gameBoard.getCoords(playerId);
  }
  public coordToBoardIndex(coord: Coord): number {
    return this.gameBoard.coordToBoardIndex(coord);
  }
  public async makeMove(source: Coord, dest: Coord): Promise<MoveResult> {
    const checkForWin = (isP1Turn: boolean): MoveResult => {
      const doCoordsFormSquare = (coords: Coord[]): boolean => {
        const allRows = coords.map((c) => c.row);
        const allCols = coords.map((c) => c.col);
        const min = new Coord(Math.min(...allRows), Math.min(...allCols));
        const max = new Coord(Math.max(...allRows), Math.max(...allCols));
        const diff = max.sub(min);
        const isSquare = diff.row === 1 && diff.col === 1;
        return isSquare;
      };
      const { gameBoard, player1Id, player2Id, player1Name, player2Name } = this;
      const playerId = isP1Turn ? player1Id : player2Id;

      const playerCoords = gameBoard.getCoords(playerId);
      const squareCoords: Coord[] = [new Coord(0, 0), new Coord(3, 0), new Coord(3, 3), new Coord(0, 3)];

      // winning conditions:
      // 1. all in one row
      // 2. all in one column
      // 3. all in corners
      // 4. all in square
      // losing conditions:
      // 1. on three sides of an opponent

      const firstPiece = playerCoords[0];
      const allInRow = playerCoords.every((pc) => pc.row === firstPiece.row);
      const allInCol = playerCoords.every((pc) => pc.col === firstPiece.col);
      const allInCorners = playerCoords.every((pc) => squareCoords.findIndex((sc) => pc.equals(sc)) >= 0);
      const allInSquare = doCoordsFormSquare(playerCoords);

      const winnerName = isP1Turn ? player1Name : player2Name;
      let winningMessage = undefined;
      if (allInRow) {
        winningMessage = `${winnerName} wins by forming a row`;
      }
      if (allInCol) {
        winningMessage = `${winnerName} wins by forming a column`;
      }
      if (allInCorners) {
        winningMessage = `${winnerName} wins by putting pieces in corners`;
      }
      if (allInSquare) {
        winningMessage = `${winnerName} wins by forming a square`;
      }
      if (winningMessage) {
        return {
          isTerminal: true,
          message: winningMessage,
        };
      } else {
        return {
          isTerminal: false,
        };
      }
    };

    const { gameBoard, player1Id, player2Id } = this;
    const isP1Turn = this.currentPlayerId === player1Id;
    const playerId = isP1Turn ? player1Id : player2Id;

    this._moveHistory.push({ playerId, source, dest });
    gameBoard.move(source, dest);

    const result = checkForWin(isP1Turn);
    if (result.isTerminal) {
      this.isGameOver = true;
    } else {
      this.currentPlayerId = isP1Turn ? this.player2Id : this.player1Id;
    }
    return result;
  }
  public getValidMoves(start: Coord): Coord[] {
    const getFarthestCoord = (startCoord: Coord, direction: Coord): Coord => {
      const { gameBoard } = this;
      let farthestPos = undefined;
      let testPos = startCoord;
      while (true) {
        const pos = new Coord(testPos.row + direction.row, testPos.col + direction.col);

        const withinBounds = gameBoard.size.contains(pos);
        if (!withinBounds) {
          break;
        }
        const isOccupied = gameBoard.get(pos) !== null;
        if (isOccupied) {
          break;
        }

        farthestPos = pos;
        testPos = pos;
      }
      return farthestPos;
    };

    const farthestCoords = Coord.allDirections
      .map((direction) => getFarthestCoord(start, direction))
      .filter((farCoord) => farCoord !== undefined);

    return farthestCoords;
  }
  public undoMove(): Move {
    if (!this.canUndoMove) {
      throw "Newp! Can't undo!";
    }
    const { gameBoard } = this;
    const move = this._moveHistory.pop();
    const { source, dest, playerId } = move;
    this.currentPlayerId = playerId;
    gameBoard.move(dest, source);
    return move;
  }
  public get canUndoMove(): boolean {
    if (this._moveHistory.length === 0 || this.isGameOver) {
      return false;
    }
    return true;
  }
}
