export interface StorageInterface<T> {
  getter: () => T;
  setter: (data: T) => void;
}
export const useSessionStorage = <T>(
  key: string,
  defaultData?: T
): StorageInterface<T> => {
  return {
    getter: () => {
      let item = sessionStorage.getItem(key);
      if (item === null && defaultData !== undefined) {
        item = JSON.stringify(defaultData);
        sessionStorage.setItem(key, item);
      }
      const data = JSON.parse(item) as T;
      return data;
    },
    setter: (data: T) => {
      const text = JSON.stringify(data as T);
      console.log(`Saving data to local storage`);
      console.log(`Key=${key}`);
      console.log(`Text=${text}`);
      sessionStorage.setItem(key, text);
    },
  };
};
