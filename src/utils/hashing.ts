export function generateHash(text: string) {
    var hash = 0;
    if (text.length == 0) {
        return hash;
    }
    for (let i = 0; i < text.length; i++) {
        var charCode = text.charCodeAt(i);
        hash = (hash << 7) - hash + charCode;
        hash = hash & hash;
    }
    return hash;
}
