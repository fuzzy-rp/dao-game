export interface Options {
    text: string;
    title?: string;
    willBackgroundCancel?: boolean;
}

let previousPromise: Promise<void> = undefined;
/*
    <div class="modal">
        <div class="modal-content">
            <span class="close">Close</span>
            <p></p>
        </div>
    </div>
*/
export const showModal = (options: Options): Promise<void> => {
    const { text, title, willBackgroundCancel } = options;
    const modal = document.createElement('div');
    modal.classList.add('modal');

    const modalContentDiv = document.createElement('div');
    modalContentDiv.classList.add('modal-content');
    modalContentDiv.classList.add('info');
    modal.appendChild(modalContentDiv);

    if (title) {
        const modelContentTitle = document.createElement('div');
        modelContentTitle.classList.add('info');
        modelContentTitle.innerText = title;
        modalContentDiv.appendChild(modelContentTitle);
    }

    const closeSpan = document.createElement('span');
    closeSpan.classList.add('close');
    closeSpan.innerText = 'x';
    modalContentDiv.appendChild(closeSpan);

    const modalContentPara = document.createElement('p');
    modalContentPara.id = 'modal-content';
    modalContentPara.innerText = text;
    modalContentDiv.appendChild(modalContentPara);

    document.body.appendChild(modal);

    const promise = new Promise<void>((resolve, reject) => {
        let isModalOpen = true;
        const closeModal = () => {
            modal.style.display = "none";
            isModalOpen = false;
            modal.parentNode.removeChild(modal);
            resolve();
        }

        // When the user clicks on <span> (x), close the modal
        closeSpan.onclick = () => closeModal();

        if (willBackgroundCancel === undefined || willBackgroundCancel === true) {
            // When the user clicks anywhere outside of the modal, close it
            window.onclick = (event: MouseEvent) => {
                if (event.target == modal) {
                    closeModal();
                }
            };
        }
    });

    // show modal
    modal.style.display = "block";

    return promise;
}