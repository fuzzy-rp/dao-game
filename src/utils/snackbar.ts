import { delay } from "../utils/delay";

export async function showSnackbar(
  text: string,
  fadeDurationMs: number = 500,
  showDurationMs: number = 3000) {

  const div = document.createElement('div');
  div.id = 'snackbar';
  div.classList.add('info');
  div.classList.add('show');
  div.innerText = text;
  document.body.appendChild(div);

  const invisibleKeyframe = {
    opacity: 0,
    bottom: '0px'
  };
  const visibleKeyframe = {
    opacity: 1,
    bottom: '30px'
  };
  const options = { duration: fadeDurationMs, iterations: 1 };
  await div.animate([invisibleKeyframe, visibleKeyframe], options).finished;
  await delay(showDurationMs);
  await div.animate([visibleKeyframe, {
    opacity: 0,
    bottom: '30px'
  }], options).finished;
  div.parentNode.removeChild(div);
}
