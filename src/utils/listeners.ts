export interface Listener {
    element: Element;
    type: string;
    listener: (event: Event) => void;
}

export class ListenerManager {
    private listeners: Listener[] = [];

    public addListener(element: Element, type: string, listener: (event: Event) => void): void {
        const { listeners } = this;
        element.addEventListener(type, listener);
        listeners.push({ element, type, listener });
    }

    public removeListeners() {
        const { listeners } = this;
        listeners.forEach(value => {
            const { element, type, listener } = value;
            element.removeEventListener(type, listener);
        });
        listeners.length = 0;
    }
}
