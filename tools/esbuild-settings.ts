import esbuild from 'esbuild';
import fs from 'fs'

export const hostPort = 3000;
const outDir = 'public/js';

export const serveOptions: esbuild.ServeOptions = {
    servedir: 'public',
};
export const buildOptions: esbuild.BuildOptions = {
    entryPoints: ['src/pages/local.ts', 'src/pages/remote.ts', 'src/pages/index.ts'],
    bundle: true,
    outdir: outDir,
    sourcemap: true,
    target: [
        'es5',
        'es2016'
    ],
    loader: {
        '.ts': 'ts'
    }
};
export function clearOutDir() {
    fs.rmSync(outDir, { recursive: true, force: true });
}