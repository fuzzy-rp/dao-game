import esbuild from 'esbuild';
import { buildOptions, clearOutDir } from './esbuild-settings'

clearOutDir();

esbuild.build(buildOptions).catch(() => process.exit(1))