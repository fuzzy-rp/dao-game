import esbuild from 'esbuild';
import { serveOptions, buildOptions, hostPort, clearOutDir } from './esbuild-settings'

clearOutDir();

esbuild.build(buildOptions).catch(() => process.exit(1))