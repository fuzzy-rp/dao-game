import esbuild from 'esbuild';
import http from 'http';
import { serveOptions, buildOptions, hostPort, clearOutDir } from './esbuild-settings'

clearOutDir();

esbuild.serve(serveOptions, buildOptions).then((result) => {
  const { host, port } = result;
  http
    .createServer((req, res) => {
      const options: http.RequestOptions = {
        hostname: host,
        port: port,
        path: req.url,
        method: req.method,
        headers: req.headers,
      };
      const proxyReq = http.request(options, (proxyRes) => {
        if (proxyRes.statusCode === 404) {
          res.writeHead(404, { 'Content-Type': 'text/html' });
          res.end('<h1>A custom 404 page</h1>');
          return;
        }

        res.writeHead(proxyRes.statusCode!, proxyRes.headers);
        proxyRes.pipe(res, { end: true });
      });

      req.pipe(proxyReq, { end: true });
    })
    .listen(hostPort, () => {
      const url = `http://localhost:${hostPort}/`;
      // childProcess.exec(`start ${url}`);
      console.log(`Server serving at ${url}`);
    });
});
