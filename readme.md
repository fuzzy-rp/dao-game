# Dao

## Info

This is a TypeScript implementation of the board game [Dao](https://en.wikipedia.org/wiki/Dao_(game)).
The whole thing was a fun project to try building a TypeScript app from scratch.

## Play

Play the game here: [https://pavelsafronov.gitlab.io/dao-game/](https://pavelsafronov.gitlab.io/dao-game/)

## Running

1. `npm run start`
2. Navigate to [`http://localhost:3000/`](http://localhost:3000/)

## Debugging

1. `npm run start`
2. Run `Launch Chrome against localhost` debug task in VSCode

## Esbuild

This project is using [esbuild](https://esbuild.github.io/getting-started/#build-scripts) for creating a bundle.

## Todos

1. Add video chat
2. Convert to React
3. Publish versions of all branches
4. Add computer opponent using simple minimax logic.
   1. [https://www.neverstopbuilding.com/blog/minimax](https://www.neverstopbuilding.com/blog/minimax)
   2. [https://en.wikipedia.org/wiki/Minimax](https://en.wikipedia.org/wiki/Minimax)
